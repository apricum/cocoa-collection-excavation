//
//  DeferredDrawingView.swift
//  Collection View Model Demo
//
//  Created by August Freytag on 03/10/2019.
//  Copyright © 2019 August S. Freytag. All rights reserved.
//

import Cocoa

class DeferredDrawingView: NSView {

	weak var targetView: NSView?
	
	override var isFlipped: Bool { true }
	
    override func draw(_ dirtyRect: NSRect) {
		guard let targetView = targetView else {
			return
		}
		
		targetView.setFrameSize(frame.size)
		targetView.draw(dirtyRect)
    }
    
}
