//
//  ScrollViewDocumentView.swift
//  Collection View Model Demo
//
//  Created by August Freytag on 18/08/2019.
//  Copyright © 2019 August S. Freytag. All rights reserved.
//

import Cocoa

class ScrollViewDocumentView: NSView {

	override var isFlipped: Bool { true }
    
}
