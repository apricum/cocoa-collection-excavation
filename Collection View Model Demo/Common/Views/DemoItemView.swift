//
//  DemoItemView.swift
//  Collection View Model Demo
//
//  Created by August Freytag on 16/08/2019.
//  Copyright © 2019 August S. Freytag. All rights reserved.
//

import Cocoa

@IBDesignable class DemoItemView: NSView {

	@IBInspectable var labelStringValue: String = "<None>"
	
	override var isFlipped: Bool { true }
	
	// MARK: Drawing
	
    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)
		
		let color = NSColor.selectedControlColor
		color.set()
		
		let cornerRadius: CGFloat = 8
		let roundedRect = NSBezierPath(roundedRect: bounds, xRadius: cornerRadius, yRadius: cornerRadius)
		roundedRect.fill()
		
		let stringAttributes: [NSAttributedString.Key: Any] = [
			NSAttributedString.Key.foregroundColor: NSColor.darkGray
		]
		
		let drawableString = NSAttributedString(string: labelStringValue, attributes: stringAttributes)
		let stringHeight = drawableString.size().height
		let stringTopOffset = (bounds.height - stringHeight) / 2
		let stringRect = CGRect(x: bounds.minX + 10, y: bounds.minY + stringTopOffset, width: bounds.width - 20, height: stringHeight)
		
		drawableString.draw(in: stringRect)
    }
    
}
