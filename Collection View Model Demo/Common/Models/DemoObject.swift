//
//  DemoModel.swift
//  Collection View Model Demo
//
//  Created by August Freytag on 24/08/2019.
//  Copyright © 2019 August S. Freytag. All rights reserved.
//

import Foundation
import CXCollectionView

struct DemoObject: CXDataObject {
	
	var objectId: Identifier { id.hashValue }
	
	let id: UUID = UUID()
	let name: String
	
}
