//
//  DemoCollectionViewItem.swift
//  Collection View Model Demo
//
//  Created by August Freytag on 16/08/2019.
//  Copyright © 2019 August S. Freytag. All rights reserved.
//

import Cocoa
import CXCollectionView

class DemoCollectionViewItem: NSCollectionViewItem, CXCollectionViewItem {
	
	var itemView: DemoItemView! { self.view as? DemoItemView }
	
	override func loadView() {
		let view = DemoItemView()
		
		view.wantsLayer = true
		view.translatesAutoresizingMaskIntoConstraints = false
		
		self.view = view
	}
    
}
