//
//  DemoItemForm.swift
//  Collection View Model Demo
//
//  Created by August Freytag on 16/08/2019.
//  Copyright © 2019 August S. Freytag. All rights reserved.
//

import Cocoa
import CXCollectionView

class DemoCollectionViewItemForm: CXCollectionViewItemForm {
	
	static let itemIdentifier = NSUserInterfaceItemIdentifier("Item")
	
	func registerCollectionViewItems(_ collectionView: CXCollectionView) {
		collectionView.register(DemoCollectionViewItem.self, forItemWithIdentifier: DemoCollectionViewItemForm.itemIdentifier)
	}
	
	func collectionView(_ collectionView: CXCollectionView, object: CXDataObject, at indexPath: IndexPath) -> NSCollectionViewItem {
		guard let viewItem = collectionView.makeItem(withIdentifier: DemoCollectionViewItemForm.itemIdentifier, for: indexPath) as? DemoCollectionViewItem else {
			fatalError("Failed to instantiate collection view item for object at index path \(indexPath), may be missing registration.")
		}
		
		let demoObject = object as! DemoObject
		viewItem.itemView.labelStringValue = "Item at \(indexPath), \(demoObject.name)"
		
		return viewItem
	}
	
}
