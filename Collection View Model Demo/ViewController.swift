//
//  ViewController.swift
//  Collection View Model Demo
//
//  Created by August Freytag on 11/08/2019.
//  Copyright © 2019 August S. Freytag. All rights reserved.
//

import Cocoa
import CXCollectionView

class ViewController: NSViewController, CXCollectionViewChangeInitiator {

	typealias DemoModeledSection = CXDataSection
	
	@IBOutlet weak var collectionView: CXCollectionView?
	@IBOutlet weak var collectionViewScrollView: CXCollectionScrollView?
	
	var changeReceivingCollectionView: CXCollectionView? { collectionView }
	var changeReceivingCollectionViewEnclosure: CXCollectionEnclosure? { collectionViewScrollView }
	
	private var itemForm: DemoCollectionViewItemForm!
	
	private var timer: Timer?
	private var sections: [CXDataSection] = []
	private var displacedEmptySections: [CXDataSection] = []
	private var displacedItems: [CXDataObject] = []
	private var isInitialRun: Bool = true
	
	private static let baseSections = [
		DemoModeledSection(),
		DemoModeledSection(),
		DemoModeledSection(),
		DemoModeledSection()
	]
	
	private static let baseItems = [
		DemoObject(name: "Alpha"),
		DemoObject(name: "Beta"),
		DemoObject(name: "Gamma"),
		DemoObject(name: "Delta"),
		DemoObject(name: "Epsilon"),
		DemoObject(name: "Zeta"),
		DemoObject(name: "Eta"),
		DemoObject(name: "Theta"),
		DemoObject(name: "Iota"),
		DemoObject(name: "Kappa"),
		DemoObject(name: "Lambda"),
		DemoObject(name: "Mu"),
		DemoObject(name: "Nu"),
		DemoObject(name: "Xi"),
		DemoObject(name: "Omicron"),
		DemoObject(name: "Pi"),
		DemoObject(name: "Rho"),
		DemoObject(name: "Sigma")
	]
	
	// MARK: Init
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.itemForm = DemoCollectionViewItemForm()
		changeReceivingCollectionView?.contentDataSource = self
		changeReceivingCollectionView?.delegate = self
		changeReceivingCollectionView?.itemForm = self.itemForm
		
		changeReceivingCollectionView?.reloadStructure()
	}
	
	override func viewDidAppear() {
		super.viewDidAppear()
		randomizeContent()
		
		evolveItems {
			self.rearmTimer()
		}
	}

	private func rearmTimer() {
		timer?.invalidate()
		timer = Timer.scheduledTimer(withTimeInterval: 1.5, repeats: false, block: { timer in
			self.evolveItems {
				self.rearmTimer()
			}
		})
	}
	
	private func evolveItems(_ didCompleteShuffle: @escaping () -> Void) {
		if !isInitialRun {
			for _ in 0 ..< 10 {
				// if Int.random(in: 0 ... 10) > 8 { shuffleItems() }
				// if Int.random(in: 0 ... 10) > 2 { displaceItems() }
				if Int.random(in: 0 ... 10) > 8 { displaceSections() }
				displaceItems()
			}
		}
		
		defer { isInitialRun = false }
		
		reloadDifference() {
			didCompleteShuffle()
		}
	}
	
	// MARK: Randomize
	
	private func randomizeContent() {
		var availableItems = ViewController.baseItems
		let numberOfItems = ViewController.baseItems.count
		let numberOfSections = ViewController.baseSections.count
		
		for var insertableSection in ViewController.baseSections {
			let range = 0 ..< numberOfItems / numberOfSections
			insertableSection.objects = Array(availableItems[range])
			availableItems.removeSubrange(range)
			sections.append(insertableSection)
		}
	}
	
	// MARK: Shuffle
	
	private func shuffleObjects() {
		guard let sectionIndex = sections.indices.randomElement() else {
			return
		}
		
		var shuffledObjects = sections[sectionIndex].objects
		
		guard shuffledObjects.count > 1 else {
			return
		}
		
		for _ in 0 ..< Int.random(in: 1 ..< 4) {
			let pickedItemIndex = shuffledObjects.indices.randomElement()!
			let item = shuffledObjects.remove(at: pickedItemIndex)
			let targetItemIndex = shuffledObjects.indices.randomElement()!
			shuffledObjects.insert(item, at: targetItemIndex)
		}
		
		sections[sectionIndex] = CXDataSection(id: sections[sectionIndex].sectionId, shuffledObjects)
	}
	
	// MARK: Section Displacement
	
	private func displaceSections() {
		// TODO: Implement emptying and displacing entire sections or adding new sections from displacement rail.
		let shouldDisplaceSection = Bool.random()
		
		if shouldDisplaceSection, !sections.isEmpty {
			displaceSingleSection()
		} else if !shouldDisplaceSection, !displacedEmptySections.isEmpty, !displacedItems.isEmpty {
			restoreSingleDisplacedSection()
		}
	}
	
	private func displaceSingleSection() {
		guard let displaceableSectionIndex = sections.indices.randomElement() else {
			return
		}
		
		var displaceableSection = sections.remove(at: displaceableSectionIndex)
		
		for item in displaceableSection {
			displacedItems.append(item)
		}
		
		displaceableSection.objects = []
		displacedEmptySections.append(displaceableSection)
	}
	
	private func restoreSingleDisplacedSection() {
		guard let displacedSectionIndex = displacedEmptySections.indices.randomElement(), displacedItems.count > 0 else {
			return
		}
		
		var insertableSection = displacedEmptySections.remove(at: displacedSectionIndex)
		
		for _ in 0 ..< Int.random(in: 0 ..< displacedItems.count) {
			let insertableItemIndex = displacedItems.indices.randomElement()!
			insertableSection.objects.append(displacedItems.remove(at: insertableItemIndex))
		}
		
		let sectionInsertionIndex = sections.indices.randomElement() ?? 0
		sections.insert(insertableSection, at: sectionInsertionIndex)
	}
	
	// MARK: Item Displacement
	
	private func displaceItems() {
		let shouldDisplaceItem = Bool.random()
		
		guard let sectionIndex = sections.indices.randomElement() else {
			return
		}
		
		if shouldDisplaceItem && sections[sectionIndex].objects.count > 1 {
			for _ in 0 ..< Int.random(in: 0 ..< sections[sectionIndex].objects.count / 2) {
				displaceSingleItem(inSectionAt: sectionIndex)
			}
			
			return
		}
		
		guard !shouldDisplaceItem, displacedItems.count > 1 else {
			return
		}
		
		for _ in 0 ..< Int.random(in: 0 ..< displacedItems.count / 2) {
			restoreSingleDisplacedItem()
		}
	}
	
	private func displaceSingleItem(inSectionAt sectionIndex: Int) {
		let displacedItemIndex = sections[sectionIndex].objects.indices.randomElement()!
		let displacedItem = sections[sectionIndex].objects.remove(at: displacedItemIndex)
		displacedItems.append(displacedItem)
	}
	
	private func restoreSingleDisplacedItem() {
		let displacedItemIndex = displacedItems.indices.randomElement()!
		let displacedItem = displacedItems.remove(at: displacedItemIndex)
		let insertionSectionIndex = sections.indices.randomElement()!
		let insertionIndex = sections[insertionSectionIndex].objects.indices.randomElement() ?? 0
		sections[insertionSectionIndex].objects.insert(displacedItem, at: insertionIndex)
	}
	
}

extension ViewController: CXCollectionViewDataSource {
	
	func collectionViewShouldAnimateReload(_ collectionView: CXCollectionView) -> Bool {
		return true
	}
	
	func collectionViewNumberOfSections(_ collectionView: CXCollectionView) -> Int {
		return sections.count
	}
	
	func collectionView(_ collectionView: CXCollectionView, sectionAt sectionIndex: Int) -> CXDataSection {
		return sections[sectionIndex]
	}
	
}

extension ViewController: NSCollectionViewDelegate {}

extension ViewController: NSCollectionViewDelegateFlowLayout {
	
	func collectionView(_ collectionView: NSCollectionView, layout collectionViewLayout: NSCollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> NSSize {
		return CGSize(width: 320, height: 24)
	}
	
	func collectionView(_ collectionView: NSCollectionView, layout collectionViewLayout: NSCollectionViewLayout, insetForSectionAt section: Int) -> NSEdgeInsets {
		return NSEdgeInsets(top: 0, left: 0, bottom: 20, right: 0)
	}
	
	func collectionView(_ collectionView: NSCollectionView, layout collectionViewLayout: NSCollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
		return 4
	}
	
	func collectionView(_ collectionView: NSCollectionView, layout collectionViewLayout: NSCollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
		return 0
	}
	
}
