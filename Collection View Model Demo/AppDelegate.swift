//
//  AppDelegate.swift
//  Collection View Model Demo
//
//  Created by August Freytag on 11/08/2019.
//  Copyright © 2019 August S. Freytag. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



	func applicationDidFinishLaunching(_ aNotification: Notification) {
		// Insert code here to initialize your application
	}

	func applicationWillTerminate(_ aNotification: Notification) {
		// Insert code here to tear down your application
	}


}

